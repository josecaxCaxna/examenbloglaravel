<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Input;
use App\Models\Image;
use App\Models\User;
use Illuminate\Support\Str;
class inputLogic extends Controller
{
    /**
     * Función para dar de alta una entrada
     * @param Request
     * @return Json Confirmation
     */
    public function createInput(Request $request)
    {
        $user = auth()->user();
        Input::create(
            [
                'title'=>$request->input('tittle'),
                'body'=>$request->input('data'),
                'slug'=>Str::slug($request->input('slug'),'-'),
                'user_id'=>$user->id
            ]
        );
        return response()->json([
            'success' => true,
            'message' => 'Created'
        ]);
    }

    /**
     * Función para crear las imagenes de cada entrada
     * @param Request
     * @return Json Confirmation
     */
    public function createInputImages(Request $request){
        $idInput = Input::orderBy('id', 'desc')->first()->id;
        $files = $request->file('file');
        foreach ($files as  $file) {
            if($files){
                $filename = $file->getClientOriginalName();
                $onlyName = pathinfo($filename, PATHINFO_FILENAME);
                $replaceName = str_replace(' ', '_' , $onlyName) . '__' . rand() . '-' .time() .'.' .$file->getClientOriginalExtension();
                $file->storeAs('public',$replaceName);
                $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/storage/' . $replaceName;
                Image::create(
                    [
                        'url'=>$baseurl,
                        'model_id'=>$idInput,
                        'model_type'=>'App\Models\Input'
                    ]
                );
            }
        }
        return response()->json([
            'success' => true,
            'message' => 'Created',
            'url' => $baseurl,
        ]);
    }

    public function listInputs(){
        $user = auth()->user();
        $data = $user->inputs;
        return response()->json([
            'success' => true,
            'message' => $data
        ]);
    }

    public function editOneInput(Input $input){
        // $input = Input::find(2);
        // $books = $input->images;
        // dd($books);
        // $idInput = Input::latest('id')->first();
    }
}

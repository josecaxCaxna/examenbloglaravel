<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Image extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $timestamps = false;
    protected $fillable = ['url','model_id', 'model_type'];
    protected $dates = ['deleted_at'];
    public function model(){
        return $this->morphTo();
    }

    public function users(){
        return $this->morphTo();
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JwtAuthController;
use App\Http\Controllers\inputLogic;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('login',[JwtAuthController::class,'login']);
Route::post('register',[JwtAuthController::class,'register']);

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::post('logout',[JwtAuthController::class,'logout']);
    Route::get('me',[JwtAuthController::class,'getAuthenticatedUser']);

    Route::post('createInput',[inputLogic::class,'createInput']);
    Route::post('createInputImages',[inputLogic::class,'createInputImages']);
    Route::get('listInputs',[inputLogic::class,'listInputs']);
});
